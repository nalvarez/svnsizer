#include <fstream>
#include <sstream>
#include <iostream>

#include <subversion-1/svn_pools.h>
#include <subversion-1/svn_repos.h>
#include <subversion-1/svn_fs.h>
#include <sys/stat.h>

typedef struct {
    int revcount;
    int shardsize;
} RepoInfo;

class SvnAutoPool {
public:
    SvnAutoPool(apr_pool_t* parent=0):
        pool(svn_pool_create(parent))
    {
    }
    SvnAutoPool(SvnAutoPool& parent):
        pool(svn_pool_create(parent.pool))
    {
    }
    ~SvnAutoPool() {
        svn_pool_destroy(pool);
    }
    inline operator apr_pool_t*() const {
        return pool;
    }
private:
    apr_pool_t* pool;
};

// returns zero on success
int parse_shard_size(const char* format_path, int* shard_size)
{
    std::ifstream fp(format_path, std::ios::in);

    if (!fp.is_open()) {
        std::cerr << "Can't open format file" << std::endl;
        return 1;
    }
    std::string line;
    std::getline(fp, line);
    // we should have the format version here now

    *shard_size = 0;

    // now read the format options
    while (std::getline(fp, line)) {
        std::istringstream iss(line);
        std::string option_name;
        iss >> option_name;
        if (option_name == "layout") {
            std::string word;
            iss >> word;
            if (word == "sharded") {
                iss >> *shard_size;
                if (*shard_size <= 0) {
                    return 1;
                }
            }
        }
    }
    fp.close();

    return 0;
}

int get_repo_info(RepoInfo* info, svn_fs_t* fs, SvnAutoPool& parent_pool)
{
    SvnAutoPool pool(parent_pool);

    svn_revnum_t maxrev;
    SVN_INT_ERR(svn_fs_youngest_rev(&maxrev, fs, pool));
    info->revcount = maxrev;

    char* format_path;
    apr_filepath_merge(&format_path, svn_fs_path(fs, pool), "format", 0, pool);
    std::cout << "Path: " << format_path << std::endl;

    if (parse_shard_size(format_path, &info->shardsize) != 0) {
        std::cerr << "Can't parse shard size" << std::endl;
        return 1;
    }
    std::cout << "Shard size is " << info->shardsize << std::endl;

    return 0;
}

void get_revision_path(std::string& path, const RepoInfo& info, const char* fs_root, int revnum) {
    std::ostringstream oss;
    oss << fs_root << "/revs/";
    if (info.shardsize != 0) {
        oss << (revnum / info.shardsize) << '/';
    }
    oss << revnum;
    path = oss.str();
}

int main(int argc, const char* const* argv)
{
    svn_repos_t* repos;
    svn_fs_t* fs;

    if (argc < 2) {
        std::cerr << "usage: " << argv[0] << "REPO_PATH" << std::endl;
        exit(1);
    }

    // initialize APR or exit
    if (apr_initialize() != APR_SUCCESS) {
        std::cerr << "apr_initialize failed()." << std::endl;
        exit(1);
    }

    SvnAutoPool pool;

    SVN_INT_ERR(svn_fs_initialize(pool));
    SVN_INT_ERR(svn_repos_open(&repos, argv[1], pool));
    fs = svn_repos_fs(repos);

    RepoInfo info;
    get_repo_info(&info, fs, pool);

    const char* fs_root = svn_fs_path(fs, pool);
    std::string path;

    unsigned long total_size = 0;
    struct stat statinfo;

    for (int revision=0; revision<=info.revcount; ++revision) {
        // we use a separate pool to avoid accumulating memory on every revision
        // due to the revprop hash
        SvnAutoPool subpool(pool);

        get_revision_path(path, info, fs_root, revision);
        if (stat(path.c_str(), &statinfo) != 0) {
            std::cerr << "Error getting info about " << path << std::endl;
            return 1;
        }
        total_size += statinfo.st_size;

        apr_hash_t *revprops;
        SVN_INT_ERR(svn_fs_revision_proplist(&revprops, fs, revision, subpool));
        svn_string_t *svndate = (svn_string_t*)apr_hash_get(revprops, "svn:date", APR_HASH_KEY_STRING);
        std::cout << revision << " " << svndate->data << " " << total_size << std::endl;

    }

    return 0;
}
